const String SERVER_URL = 'socket--node-chat.herokuapp.com';
const String SERVER_URL_HTTPS = 'https://socket--node-chat.herokuapp.com';
// const String SERVER_URL = 'http://10.0.2.2:3000';
// const String SERVER_URL = 'http://192.168.0.81:3000';

const String SERVER_NAMESPACE = '/';
